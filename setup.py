from setuptools import setup, find_packages

setup(
    name="mhtml2html",
    version="1.0",
    description="Conversion from MHT(ML) format to completely self-contained HTML",
    author=['Thomas McWork'],
    author_email='thomas.mc.work@gmail.com',
    url='https://gitlab.com/thomas.mc.work/mhtml2html',
    license='AGPL3',

    py_modules=['mhtml2html'],
    python_requires='>=3',
    install_requires=[
        'beautifulsoup4',
    ],

    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'mhtml2html=mhtml2html:cli_entrypoint'
        ]
    },
    zip_safe=True
)
