# mhtml2html

Conversion from MHT(ML) format to completely self-contained HTML.

## Features

- embed all attached and referenced resources (images, css, …)
- remove all `<script>`  tags
- remove all `<link rel="dns-prefetch" …>`  tags

## Installation

### Requirements

- Python 3
- pip

### Setup

    pip install git+https://gitlab.com/thomas.mc.work/mhtml2html

## Usage

    mhtml2html path/to/file.mht

A corresponding output file will be created (and replaced without confirmation): `path/to/file.mht.html`

All dropped attachments are printed to `stderr` on execution. Feel free to file an issue if you spot something that should be included. Dont' forget to add the mhtml file.

## Development

### Dependency Management

This project is using [Pipenv](https://docs.pipenv.org/) — "the officially recommended Python packaging tool from 
Python.org" and thus the designated successor of pip and virtualenv.

### Testing

Have a look at the test files repository: https://gitlab.com/thomas.mc.work/mhtml2html-test-files

These files are required to be working flawlessly.
